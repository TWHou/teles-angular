'use strict';

angular.module('telesApp', ['ui.router', 'ngResource', 'ngDialog', 'textAngular'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

    // route for the home page
      .state('app', {
      url: '/',
      views: {
        'header': {
          templateUrl: 'views/header.html',
          controller: 'HeaderController'
        },
        'content': {
          templateUrl: 'views/aboutus.html',
          controller: 'AboutController'
        },
        'footer': {
          templateUrl: 'views/footer.html',
        }
      }

    })

    // route for the aboutus page
    .state('app.aboutus', {
      url: 'aboutus',
      views: {
        'content@': {
          templateUrl: 'views/aboutus.html',
          controller: 'AboutController'
        }
      }
    })

    // route for the contactus page
    .state('app.contactus', {
      url: 'contactus',
      views: {
        'content@': {
          templateUrl: 'views/contactus.html',
          controller: 'ContactController'
        }
      }
    })

    // route for the event page
    .state('app.event', {
      url: 'event',
      views: {
        'content@': {
          templateUrl: 'views/event.html',
          controller: 'EventController'
        }
      }
    })

    // route for the eventdetail page
    .state('app.event.eventdetail', {
      url: '/:id',
      views: {
        'content@': {
          templateUrl: 'views/eventdetail.html',
          controller: 'EventDetailController'
        }
      }
    })

    // route for the publication page
    .state('app.publication', {
      url: 'publication',
      views: {
        'content@': {
          templateUrl: 'views/publication.html',
          controller: 'PubController'
        }
      }
    })

    // route for the pubdetail page
    .state('app.publication.pubdetail', {
      url: '/:id',
      views: {
        'content@': {
          templateUrl: 'views/pubdetail.html',
          controller: 'PubDetailController'
        }
      }
    })

    // route for the forum page
    .state('app.forum', {
      url: 'forum',
      views: {
        'content@': {
          templateUrl: 'views/forum.html',
          controller: 'EventController'
        }
      }
    })

    // route for the category page
    .state('app.forum.category', {
      url: '/category/:id',
      views: {
        'content@': {
          templateUrl: 'views/category.html',
          controller: 'CategoryController'
        }
      }
    })

    // route for the post page
    .state('app.forum.post', {
      url: '/post/:id',
      views: {
        'content@': {
          templateUrl: 'views/post.html',
          controller: 'PostController'
        }
      }
    });

    $urlRouterProvider.otherwise('/');
  });
