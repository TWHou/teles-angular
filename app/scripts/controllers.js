// EventController: event.title, event.date, event.description, event.detail, event._id
// PubController: toggleDetail(), pub.title, pub.author, pub.image, pub.label, pub.price, pub.description, pub._id, pub.detail


'use strict';

angular.module('telesApp')

.controller('EventController', ['$scope', 'eventFactory', function ($scope, eventFactory) {

  $scope.showEvent = false;
  $scope.message = "Loading ...";

  eventFactory.query(
    function (response) {
      $scope.events = response;
      $scope.showEvent = true;

    },
    function (response) {
      $scope.message = "Error: " + response.status + " " + response.statusText;
    });

}])

.controller('ContactController', ['$scope', 'messageFactory', function ($scope, messageFactory) {

  $scope.contact = {
    name: "",
    message: "",
    email: "",
    time: ""
  };

  $scope.sendMessage = function () {
    $scope.contact.time = new Date();
    messageFactory.save($scope.contact);
    // Clear Message Form
    $scope.contact = {
      name: "",
      message: "",
      email: "",
      time: ""
    };
    $scope.contactForm.$setPristine();

  };
}])

.controller('EventDetailController', ['$scope', '$state', '$stateParams', 'eventFactory', function ($scope, $state, $stateParams, eventFactory) {

  $scope.event = {};
  $scope.showEvent = false;
  $scope.message = "Loading ...";

  $scope.event = eventFactory.get({
      id: $stateParams.id
    })
    .$promise.then(
      function (response) {
        $scope.event = response;
        $scope.showEvent = true;
      },
      function (response) {
        $scope.message = "Error: " + response.status + " " + response.statusText;
      }
    );
}])

.controller('PubController', ['$scope', 'pubFactory', function ($scope, pubFactory) {
  // PubController: toggleDetail(), pub.title, pub.author, pub.image, pub.label, pub.price, pub.description, pub._id, pub.detail

  $scope.showPub = false;
  $scope.showDetail = false;
  $scope.message = "Loading ...";

  $scope.toggleDetail = function () {
    $scope.showDetail = !$scope.showDetail;
  };

  pubFactory.query(
    function (response) {
      $scope.pubs = response;
      $scope.showPub = true;

    },
    function (response) {
      $scope.message = "Error: " + response.status + " " + response.statusText;
    });

}])

.controller('PubDetailController', ['$scope', '$state', '$stateParams', 'pubFactory', function ($scope, $state, $stateParams, pubFactory) {

  $scope.pub = {};
  $scope.showPub = false;
  $scope.message = "Loading ...";

  pubFactory.get({
      id: $stateParams.id
    })
    .$promise.then(
      function (response) {
        $scope.pub = response;
        $scope.showPub = true;
      },
      function (response) {
        $scope.message = "Error: " + response.status + " " + response.statusText;
      }
    );
}])

.controller('AboutController', ['$scope', 'boardFactory', function ($scope, boardFactory) {

  $scope.leaders = boardFactory.query();

}])

.controller('HeaderController', ['$scope', '$state', '$rootScope', 'ngDialog', 'AuthFactory', 'eventFactory', 'pubFactory', function ($scope, $state, $rootScope, ngDialog, AuthFactory, eventFactory, pubFactory) {

  $scope.loggedIn = false;
  $scope.username = '';

  if (AuthFactory.isAuthenticated()) {
    $scope.loggedIn = true;
    $scope.username = AuthFactory.getUsername();
  }

  $scope.openLogin = function () {
    ngDialog.open({
      template: 'views/login.html',
      scope: $scope,
      className: 'ngdialog-theme-default',
      controller: "LoginController"
    });
  };

  $scope.openEventAdmin = function () {
    ngDialog.open({
      template: 'views/eventadmin.html',
      width: "75%",
      scope: $scope,
      className: 'ngdialog-theme-default',
      controller: "EventAdminController"
    });
  };

  $scope.deleteEvent = function() {
    var message = '<div class="ngdialog-message">\n';
        message +='<h3>Are You Sure?</h3>\n';
        message +='<p>This action cannot be reverted.</p>\n';
        message +='</div>\n';
        message +='<div class="ngdialog-buttons">\n';
        message +='<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">YES, delete this event</button>\n';
        message +='<button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">NO! I\'m here by mistake.</button>\n';
        message +='</div>\n';
    ngDialog.openConfirm({
      template: message,
      plain: 'true'
    }).then(function () {
      eventFactory.remove({id: $stateParams.id});
      $state.go($state.current, {}, {reload: true});
    });
  };

  $scope.openPubAdmin = function () {
    ngDialog.open({
      template: 'views/pubadmin.html',
      width: "75%",
      scope: $scope,
      className: 'ngdialog-theme-default',
      controller: "PubAdminController"
    });
  };

  $scope.deletePub = function() {
    var message = '<div class="ngdialog-message">\n';
        message +='<h3>Are You Sure?</h3>\n';
        message +='<p>This action cannot be reverted.</p>\n';
        message +='</div>\n';
        message +='<div class="ngdialog-buttons">\n';
        message +='<button type="button" class="ngdialog-button ngdialog-button-primary" ng-click="confirm()">YES, delete this event</button>\n';
        message +='<button type="button" class="ngdialog-button ngdialog-button-secondary" ng-click="closeThisDialog()">NO! I\'m here by mistake.</button>\n';
        message +='</div>`;\n';
    ngDialog.openConfirm({
      template: message,
      plain: 'true'
    }).then(function () {
      pubFactory.remove({id: $stateParams.id});
      $state.go($state.current, {}, {reload: true});
    });
  };

  $scope.showMsg = function () {
    ngDialog.open({
      template: 'views/message.html',
      width: "75%",
      scope: $scope,
      className: 'ngdialog-theme-default',
      controller: "MessageController"
    });
  };

  $scope.logOut = function () {
    AuthFactory.logout();
    $scope.loggedIn = false;
    $scope.username = '';
    $scope.admin = false;
  };

  $rootScope.$on('login:Successful', function () {
    $scope.loggedIn = AuthFactory.isAuthenticated();
    $scope.username = AuthFactory.getUsername();
    $scope.admin = AuthFactory.isAdmin();
  });

  $rootScope.$on('registration:Successful', function () {
    $scope.loggedIn = AuthFactory.isAuthenticated();
    $scope.username = AuthFactory.getUsername();
  });

  $scope.stateis = function (curstate) {
    return $state.is(curstate);
  };

}])

.controller('MessageController', ['$scope', '$state', 'ngDialog', 'messageFactory', function ($scope, $state, ngDialog, messageFactory) {

  $scope.showMsg = false;
  $scope.message = "Loading ...";
  messageFactory.query(
    function (response) {
      $scope.msgs = response;
      $scope.showMsg = true;
    },
    function (response) {
      $scope.message = "Error: " + response.status + " " + response.statusText;
    });

  $scope.toggleRead = function(msgid){
    messageFactory.get({id:msgid}).$promise.then(
      function(response) {
        response.unread = !response.unread;
        messageFactory.update({id:msgid}, response);
      }
    );
    $state.go($state.current, {}, {reload: true});
  };

  $scope.deleteMsg = function(msgid){
    messageFactory.remove({id:msgid});
    $state.go($state.current, {}, {reload: true});
  };

}])

.controller('EventAdminController', ['$scope', '$state', 'ngDialog', 'eventFactory', '$stateParams', function ($scope, $state, ngDialog, eventFactory, $stateParams) {

  var original = {};

  if($stateParams.id) {
    $scope.action = "Edit Event";
    $scope.event = eventFactory.get({
      id: $stateParams.id
    })
    .$promise.then(
      function (response) {
        response.date = new Date(Date.parse(response.date));
        $scope.event = response;
        original = response;
      }
    );
  } else {
    $scope.action = "Add Event";
    $scope.event = {
      title: '',
      date: new Date(),
      description: '',
      detail: ''
    };
  }

  $scope.eventAdmin = function(){

    if($stateParams.id) {
      var updated = {};
      for (var prop in original) {
        if (original.prop !== $scope.event.prop) {
          updated.prop = $scope.event.prop;
        }
      }
      eventFactory.update(updated);
    } else {
      eventFactory.save($scope.event);
    }

    ngDialog.close();
    $state.go($state.current, {}, {reload: true});

  };

}])

.controller('PubAdminController', ['$scope', '$state', 'ngDialog', 'pubFactory', '$stateParams', function ($scope, $state, ngDialog, pubFactory, $stateParams) {

  var original = {};

  if($stateParams.id) {
    $scope.action = "Edit Publication";
    $scope.event = pubFactory.get({
      id: $stateParams.id
    })
    .$promise.then(
      function (response) {
        $scope.pub = response;
        original = response;
      }
    );
  } else {
    $scope.action = "Add Publication";
    $scope.pub = {
      title: '',
      author: '',
      image: '',
      label: '',
      price: '',
      description: '',
      detail: ''
    };
  }

  $scope.pubAdmin = function(){

    if($stateParams.id) {
      var updated = {};
      for (var prop in original) {
        if (original.prop !== $scope.pub.prop) {
          updated.prop = $scope.pub.prop;
        }
      }
      pubFactory.update(updated);
    } else {
      pubFactory.save($scope.pub);
    }

    ngDialog.close();
    $state.go($state.current, {}, {reload: true});

  };

}])

.controller('LoginController', ['$scope', 'ngDialog', '$localStorage', 'AuthFactory', function ($scope, ngDialog, $localStorage, AuthFactory) {

  $scope.loginData = $localStorage.getObject('userinfo', '{}');

  $scope.doLogin = function () {
    if ($scope.rememberMe)
      $localStorage.storeObject('userinfo', $scope.loginData);

    AuthFactory.login($scope.loginData);

    ngDialog.close();

  };

  $scope.openRegister = function () {
    ngDialog.open({
      template: 'views/register.html',
      scope: $scope,
      className: 'ngdialog-theme-default',
      controller: "RegisterController"
    });
  };

}])

.controller('RegisterController', ['$scope', 'ngDialog', '$localStorage', 'AuthFactory', function ($scope, ngDialog, $localStorage, AuthFactory) {

  $scope.register = {};
  $scope.loginData = {};

  $scope.doRegister = function () {
    console.log('Doing registration', $scope.registration);

    AuthFactory.register($scope.registration);

    ngDialog.close();

  };
}]);
