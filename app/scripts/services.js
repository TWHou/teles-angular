'use strict';

angular.module('telesApp')
  .factory('eventFactory', ['$resource', function ($resource) {
    // event.title, event.icon, event.date, event.description, event.detail, event._id

    return $resource("/events/:id", null, {
      'update': {
        method: 'PUT'
      }
    });

}])

.factory('pubFactory', ['$resource', function ($resource) {
  // pub.title, pub.author, pub.image, pub.label, pub.price, pub.description, pub._id, pub.detail
  return $resource("/publications/:id", null, {
    'update': {
      method: 'PUT'
    }
  });

}])

.factory('boardFactory', ['$resource', function ($resource) {

  // leader.image, leader.name, leader.title,  leader.bio
  return $resource("/leaders/:id", null, {
    'update': {
      method: 'PUT'
    }
  });

}])

.factory('messageFactory', ['$resource', function ($resource) {

  return $resource("/message/:id", null, {
    'update': {
      method: 'PUT'
    }
  });

}])

.factory('$localStorage', ['$window', function ($window) {
  return {
    store: function (key, value) {
      $window.localStorage[key] = value;
    },
    get: function (key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    remove: function (key) {
      $window.localStorage.removeItem(key);
    },
    storeObject: function (key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function (key, defaultValue) {
      return JSON.parse($window.localStorage[key] || defaultValue);
    }
  }
}])

.factory('AuthFactory', ['$resource', '$http', '$localStorage', '$rootScope', '$window', 'ngDialog', function ($resource, $http, $localStorage, $rootScope, $window, ngDialog) {

  var authFac = {};
  var TOKEN_KEY = 'Token';
  var isAuthenticated = false;
  var username = '';
  var authToken = undefined;
  var admin = false;


  function loadUserCredentials() {
    var credentials = $localStorage.getObject(TOKEN_KEY, '{}');
    if (credentials.username != undefined) {
      useCredentials(credentials);
    }
  }

  function storeUserCredentials(credentials) {
    $localStorage.storeObject(TOKEN_KEY, credentials);
    useCredentials(credentials);
  }

  function useCredentials(credentials) {
    isAuthenticated = true;
    username = credentials.username;
    authToken = credentials.token;
    admin = credentials.admin;

    // Set the token as header for your requests!
    $http.defaults.headers.common['x-access-token'] = authToken;
  }

  function destroyUserCredentials() {
    authToken = undefined;
    username = '';
    isAuthenticated = false;
    admin = false;
    $http.defaults.headers.common['x-access-token'] = authToken;
    $localStorage.remove(TOKEN_KEY);
  }

  authFac.login = function (loginData) {

    $resource("/users/login")
      .save(loginData,
        function (response) {
          storeUserCredentials({
            username: loginData.username,
            token: response.token,
            admin: response.admin
          });
          $rootScope.$broadcast('login:Successful');
        },
        function (response) {
          isAuthenticated = false;

          var message = '\
                <div class="ngdialog-message">\
                <div><h3>Login Unsuccessful</h3></div>' +
            '<div><p>' + response.data.err.message + '</p><p>' +
            response.data.err.name + '</p></div>' +
            '<div class="ngdialog-buttons">\
                    <button type="button" class="ngdialog-button ngdialog-button-primary" ng-click=confirm("OK")>OK</button>\
                </div>'

          ngDialog.openConfirm({
            template: message,
            plain: 'true'
          });
        }

      );

  };

  authFac.logout = function () {
    $resource("/users/logout").get(function (response) {});
    destroyUserCredentials();
  };

  authFac.register = function (registerData) {

    $resource("/users/register")
      .save(registerData,
        function (response) {
          authFac.login({
            username: registerData.username,
            password: registerData.password
          });
          if (registerData.rememberMe) {
            $localStorage.storeObject('userinfo', {
              username: registerData.username,
              password: registerData.password
            });
          }

          $rootScope.$broadcast('registration:Successful');
        },
        function (response) {

          var message = '\
                <div class="ngdialog-message">\
                <div><h3>Registration Unsuccessful</h3></div>' +
            '<div><p>' + response.data.err.message +
            '</p><p>' + response.data.err.name + '</p></div>';

          ngDialog.openConfirm({
            template: message,
            plain: 'true'
          });

        }

      );
  };

  authFac.isAuthenticated = function () {
    return isAuthenticated;
  };

  authFac.getUsername = function () {
    return username;
  };
  authFac.isAdmin = function() {
    return admin;
  };

  loadUserCredentials();

  return authFac;

}]);
